# Event Stream Handler for champs.fr API

This Python script interacts with the champs.fr API to fetch live event data during a streaming match and updates the associated widgets on Overlays.uno platform accordingly. It's designed to handle different match events such as goals, yellow cards, and red cards, updating scoreboards and other visual elements dynamically.

## Features

- Fetch live match events from champs.fr API.
- Dynamically update scoreboards and halftime overlays on Overlays.uno.
- Handle specific actions like goals, yellow cards, red cards, and end of half-time with visual feedback.

## Prerequisites

Before running this script, ensure you have the following installed:
- Python 3
- `requests` library
- `loguru` library for logging

You can install the necessary Python libraries using pip:

```bash
pip install requests loguru
```

## Configuration

1. **API URLs and Keys**: The script requires URLs for the champs.fr API and the widgets from Overlays.uno. These should be provided in a configuration JSON file. Example structure of the config file:

```json
{
    "url_champs": "https://api.champs.fr/alpha/",
    "widget_overlay_scoreboard": "https://app.overlays.uno/apiv2/controlapps/<YourWidgetID>/api",
    "widget_overlay_half_time": "https://app.overlays.uno/apiv2/controlapps/<YourOtherWidgetID>/api"
}
```
2. **Team Mapping and Display Settings**: Customize how teams and events are displayed in the widgets through the script's mapping dictionaries.
3. **Widget Links**: Scoreboard Widget: 
   - [Soccer Scorebug - Fresh](https://overlays.uno/library/150-Soccer-Scorebug---Fresh)
   - [Halftime Widget: Lower Third - Fresh](https://overlays.uno/library/179-Lower-Third---Fresh)

## Usage

To run the script, you will need to pass the match code and the path to your configuration file as command-line arguments:

```bash
python3 main.py <code_match> <url_config_file>
```
- `<code_match>`: The unique match code to fetch events for.
- `<url_config_file>`: Path to the JSON configuration file containing API URLs and keys.

The script will start fetching events for the specified match and update the widgets based on the events occurring during the match.

## Logging
The script uses `loguru` for logging events and errors. This will help in debugging and tracking the flow of data and updates.

## License

This project is open-sourced under the MIT license.

