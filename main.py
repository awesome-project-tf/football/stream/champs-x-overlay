import json
import sys
import requests
import threading
import loguru
import time

# URL = "https://api.champs.fr/alpha/"
#
# OVERLAY_SCOREBOARD = "https://app.overlays.uno/apiv2/controlapps/609apTpWe3tGh5FyKBebz8/api"
# OVERLAY_HALF_TIME = "https://app.overlays.uno/apiv2/controlapps/57kRce2svzS2V8DR60fAyx/api"

HEADERS_OVERLAYS = {
    "Content-Type": "application/json",
}

CURRENT_INDEX = 0

MAP_TEAM = {
    "A": "FCH LENS",  # Home Team
    "B": "USO LENS",  # Away Team
}

MAP_LOCAL = {
    "A": "Home",
    "B": "Away",
}

MAP_DROPDOWN = {
    "carton_jaune": 'YellowCard',
    "carton_rouge": 'RedCard',
    "but": 'Goal',
}

SCORE_MAP = {
    "Home": 0,
    "Away": 0,
}


def action_nothing(event, config):
    loguru.logger.info(f"We do nothing for the event {event['action']['description']}")


def action_dropdown(event, config):
    loguru.logger.info(event['action']['description'])

    payload = get_payload_for(event)
    loguru.logger.info(payload)

    response = requests.request("PUT", config["widget_overlay_scoreboard"], headers=HEADERS_OVERLAYS, data=payload)
    if response.status_code != 200:
        loguru.logger.error(f"Status code: {response.status_code}")
        loguru.logger.error(f"Response text: {response.text}")
        return
    toggle_dropdown(config)
    execute_after_n_seconds(3, hide_dropdown, config)


def action_but(event, config):
    action_dropdown(event, config)
    loguru.logger.info(f"We update the score for {MAP_LOCAL[event['action']['equipe']]}")
    action = f"IncreaseGoals{MAP_LOCAL[event['action']['equipe']]}"

    SCORE_MAP[MAP_LOCAL[event['action']['equipe']]] += 1

    payload = json.dumps({
        "command": action,
    })

    response = requests.request("PUT", config["widget_overlay_scoreboard"], headers=HEADERS_OVERLAYS, data=payload)
    if response.status_code != 200:
        loguru.logger.error(f"Status code: {response.status_code}")
        loguru.logger.error(f"Response text: {response.text}")
        return

    payload_but = {
        "command": "SetOverlayContent",
        "id": "c5ba9cc6-1ea9-4aff-a3fa-7486323b6404",
        "content": {
            "Height": 100,
            "Horizontal Position": 0,
            "Horizontal Position 1": "0.0",
            "Logo Fit": "contain",
            "Team 1 Color": "eae223",
            "Team 1 Logo": "//image.singular.live/e43cd0f50c48b32a828ea72060f361e7/images/2TQ3uvw1T6BqTmHls0jK9D_w312h275.png",
            "Team 1 Name": MAP_TEAM["A"],
            "Team 1 Score": SCORE_MAP['Home'],
            "Team 2 Color": "c50000",
            "Team 2 Logo": "//image.singular.live/e43cd0f50c48b32a828ea72060f361e7/images/47QJcXEs4hDlEDen3AyEHi_w958h958.png",
            "Team 2 Name": MAP_TEAM["B"],
            "Team 2 Score": SCORE_MAP['Home'],
            "Title": "Mi Temps - District artois - D5",
            "Vertical Position": 34.5,
            "Vertical Position 1": "-8.0",
            "Width": 100
        }
    }

    response = requests.request("PUT", config["widget_overlay_half_time"], headers=HEADERS_OVERLAYS, data=payload_but)
    if response.status_code != 200:
        loguru.logger.error(f"Status code: {response.status_code}")
        loguru.logger.error(f"Response text: {response.text}")
        return


def action_end_half_time(event, config):
    payload = json.dumps({
        {
            "command": "SetPeriod",
            "value": "p2"
        }
    })
    response = requests.request("PUT", config["widget_overlay_half_time"], headers=HEADERS_OVERLAYS, data=payload)
    if response.status_code != 200:
        loguru.logger.error(f"Status code: {response.status_code}")
        loguru.logger.error(f"Response text: {response.text}")
        return


def get_payload_for(event):
    payload = json.dumps({
        "command": "SetDropdownPayload",
        "value": {
            "DropdownType": MAP_DROPDOWN[event['action']['type']],
            "PlayerNumberA": str(event['action']['numero_joueur']),
            "PlayerNumberB": "02",
            "PlayerNameA": f"{MAP_TEAM[event['action']['equipe']]} {event['action'].get('nom_joueur', '')}",
            "PlayerNameB": "FCH - NAME B"
        }
    })
    return payload


def toggle_dropdown(config):
    toggle_dropdown_payload = json.dumps({
        "command": "ToggleDropdown",
    })
    response = requests.request("PUT", config["widget_overlay_scoreboard"], headers=HEADERS_OVERLAYS,
                                data=toggle_dropdown_payload)
    if response.status_code != 200:
        loguru.logger.error(f"Status code: {response.status_code}")
        loguru.logger.error(f"Response text: {response.text}")


def hide_dropdown(*args):
    config=args[0]
    hide_dropdown_payload = json.dumps({
        "command": "HideDropdown",
    })
    loguru.logger.debug(config)
    response = requests.request("PUT", config["widget_overlay_scoreboard"], headers=HEADERS_OVERLAYS,
                                data=hide_dropdown_payload)

    if response.status_code != 200:
        loguru.logger.error(f"Status code: {response.status_code}")
        loguru.logger.error(f"Response text: {response.text}")


def execute_after_n_seconds(n, f, *args):
    time.sleep(n)
    f(*args)


MAP_ACTION = {
    "nothing": action_nothing,
    "carton_jaune": action_dropdown,
    "carton_rouge": action_dropdown,
    "but": action_but,
    "fin_de_mi-temps": action_end_half_time
}


def get_match_info(code_match, config):
    url = f"{config['url_champs']}matches/{code_match}"
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()


def get_new_event(code_match, config):
    url = f"{config['url_champs']}eventswithid/{code_match}"
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()


def update(code_match, config):
    global CURRENT_INDEX
    loguru.logger.info(f"Updating from {code_match}")
    events = get_new_event(code_match, config)
    for event in events[CURRENT_INDEX:]:
        MAP_ACTION.get(event["action"]["type"], action_nothing)(event, config)
    CURRENT_INDEX = len(events)
    threading.Timer(5, update, args=(code_match, config)).start()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        sys.exit("Usage: python3 main.py <code_match> <url_config_file>")
    code_match = sys.argv[1]
    config = json.load(open(sys.argv[2]))
    match = get_match_info(code_match, config)
    threading.Timer(5, update, args=(code_match, config)).start()
